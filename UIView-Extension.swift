//
//  UIView-Extension.swift
//  HiddenChessGame
//
//  Created by Hung Hoang on 12/10/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
extension UIView {
    func makeRoundView(){
        self.layer.masksToBounds = true;
        self.layer.cornerRadius = self.width/2
        self.layer.borderWidth = 2
    }
}
