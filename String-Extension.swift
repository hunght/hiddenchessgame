//
//  String-Extension.swift
//  HiddenChessGame
//
//  Created by Hung Hoang on 11/14/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import Foundation

extension String
{
    func convertToDictionary () -> [String: Int]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Int]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func convertToAnyObjectDictionary () -> [String: Any?]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any?]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    static func getFENString() -> String {
        let whitePieces = ["r","n","p","b","q"]
        
        var FENStr = ""
        for i in 1...8{
            if i == 5 {
                FENStr += "k"
            }else{
                let dice = Int(arc4random_uniform(UInt32(whitePieces.count)));
                FENStr += whitePieces[dice]
            }
        }
        FENStr += "/"
        for _ in 1...8{
            let dice = Int(arc4random_uniform(UInt32(whitePieces.count)));
            FENStr += whitePieces[dice]
        }
        FENStr += "/8/8/8/8/"
        
        let blackPieces = ["R","N","P","B","Q"]
        
        for _ in 1...8{
            let dice = Int(arc4random_uniform(UInt32(blackPieces.count)));
            FENStr += blackPieces[dice]
        }
        
        FENStr += "/"
        
        for i in 1...8{
            if i == 5 {
                FENStr += "K"
            }else{
                let dice = Int(arc4random_uniform(UInt32(blackPieces.count)));
                FENStr += blackPieces[dice]
            }
        }
        FENStr += " w - - 0 1"
        return FENStr
    }
}
