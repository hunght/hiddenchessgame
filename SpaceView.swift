//
//  PieceButton.swift
//  HiddenChessGame
//
//  Created by Hung Hoang on 11/2/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class SpaceView: UIButton{
    public var square: Square!
    public var viewModel: Board.Space!
    private var photoView: UIImageView!
    
    private var _hiddenPiece: Piece!
    var hiddenPiece: Piece!{
        set{
            if (newValue != nil) {
                if photoView == nil {
                    
                    photoView = UIImageView.init(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: self.width*0.7,height: self.height*0.7)))
                    photoView.center = self.center
                    photoView.isUserInteractionEnabled = false
                    photoView.contentMode = .scaleAspectFit;
                    photoView.makeRoundView()
                    
                    self.addSubview(photoView)
                }
                if !newValue.color.isBlack {
                    photoView.backgroundColor = UIColor.init(hexString: "#2F1849")
                    photoView.layer.borderColor = UIColor.white.cgColor
                }else{
                    photoView.backgroundColor = UIColor.white
                    photoView.layer.borderColor = UIColor.init(hexString: "#2F1849").cgColor
                }

            }else{
                if (photoView != nil) {
                    photoView.removeFromSuperview()
                    photoView = nil
                }
                
            }
            _hiddenPiece = newValue }
        
        get{return _hiddenPiece}
    }
    
    
    
    init(frame:CGRect, viewModel: Board.Space, hiddenPiece: Piece!) {
        super.init(frame:frame)
        
        self.hiddenPiece = hiddenPiece
        self.updateView(space: viewModel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func getImageStr(piece: Piece!)-> String{
        var imageStr = piece.color.isBlack ? "b" : "w"
        switch piece.kind {
        case .pawn:
            imageStr += "p"
        case .knight:
            imageStr += "n"
        case .bishop:
            imageStr += "b"
        case .king:
            imageStr += "k"
        case .queen:
            imageStr += "q"
        case .rook:
            imageStr += "r"
        }
        return imageStr
    }
    
    public func updateView(space:Board.Space){
        viewModel = space
        square = space.square
        
        if self.hiddenPiece == nil {
            if let piece = space.piece{
                
                self.setImage(UIImage.init(named: getImageStr(piece: piece)), for: .normal)
            }else{
                
                self.setImage(nil, for: .normal)
            }
        }else{
            self.setImage(nil, for: .normal)
            if let piece = space.piece{
                photoView.image = UIImage.init(named: getImageStr(piece: piece))
            }
        }
    }
    
}
