//
//  GameViewModel.swift
//  HiddenChessGame
//
//  Created by Hung Hoang on 11/12/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import Foundation


enum GameMode:String {
    case NormalMode
    case HiddenMode
    case RandomMode
}

class GameViewModel{
    var gameMode: GameMode!
    var game: Game!
    var hiddenPieceDict: [String: Int]!
    var lastPlayerUDID:String!
    var loser: String!
    public weak var currentPlayer:Player!
    public weak var opponentPlayer:Player!
    func encode() -> URL
    {
        let baseURL = "www.hiddenchessgame.com/game"
        
        guard var components = URLComponents(string: baseURL) else
        {
            fatalError("Invalid base url")
        }
        
        var items = [URLQueryItem]()
        
        let fenStr = self.game.position.fen()
        
        print("fen string === " + fenStr)

        items.append(URLQueryItem(name: "FEN_STRING", value: fenStr))
        if (lastPlayerUDID != nil) {
            items.append(URLQueryItem(name: "lastPlayerUDID", value: lastPlayerUDID))
        }

        items.append(URLQueryItem(name: "whitePlayer", value: game.whitePlayer.toJSONString()))
        
        
        items.append(URLQueryItem(name: "blackPlayer", value: game.blackPlayer.toJSONString()))
        
        
        if (loser != nil) {
            items.append(URLQueryItem(name: "loser", value: loser))
        }
        
        items.append(URLQueryItem(name: "gameMode", value: gameMode.rawValue))

        if gameMode == .HiddenMode{
            if let dicStr = hiddenPieceDict.convertToString() {
                items.append(URLQueryItem(name: "hiddenPieceDict", value: dicStr))
            }
        }
        
        components.queryItems = items
        
        guard let url = components.url else
        {
            fatalError("Invalid URL components")
        }
        
        return url
    }
    
    init(from url: URL)
    {
        // Parse the url
        guard let components = URLComponents(url: url, resolvingAgainstBaseURL: false),
            let componentItems = components.queryItems else
        {
            fatalError("Invalid message url")
        }
        let fenQueryItem = componentItems.filter({ $0.name == "FEN_STRING" }).first!

        let fenStr = fenQueryItem.value!
        
        
        let lastPlayerUDIDQueryItem = componentItems.filter({ $0.name == "lastPlayerUDID" }).first!
        
        self.lastPlayerUDID = lastPlayerUDIDQueryItem.value!
        
        let arrLoser = componentItems.filter({ $0.name == "loser" })
        if arrLoser.count > 0 {
            let loserUDIDQueryItem = arrLoser.first!
            
            let loser = loserUDIDQueryItem.value!
            
            if !loser.isEmpty {
                self.loser = loser
            }
        }
        
        let gameModeQueryItem = componentItems.filter({ $0.name == "gameMode" }).first!
        
        self.gameMode = GameMode(rawValue: gameModeQueryItem.value!)!

        if gameMode == .HiddenMode{
            let hiddenQueryItem = componentItems.filter({ $0.name == "hiddenPieceDict" }).first!

            if let hiddenDicStr = String(hiddenQueryItem.value!)
            {
                self.hiddenPieceDict = hiddenDicStr.convertToDictionary()!
            }
        }

        let position =  Game.Position.init(fen:fenStr)
        do {
            try  self.game = Game.init(position: position!, whitePlayer: Player(), blackPlayer: Player(), variant: .standard)
            
        } catch let excuError as NSError{
            print(excuError)
        }
        
        if (game != nil){
            let whitePlayerQueryItemArr = componentItems.filter({ $0.name == "whitePlayer" })
            if  whitePlayerQueryItemArr.count > 0 {
                let whitePlayerQueryItem = whitePlayerQueryItemArr.first!
                
                let whitePlayerStr = whitePlayerQueryItem.value!
                
                if !whitePlayerStr.isEmpty {
                    game.whitePlayer = Player(JSONString:whitePlayerStr)!
                }
            }
            
            let blackPlayerQueryItemArr = componentItems.filter({ $0.name == "blackPlayer" })
            if blackPlayerQueryItemArr.count > 0 {
                
                let blackPlayerQueryItem = blackPlayerQueryItemArr.first!
                
                let blackPlayerStr = blackPlayerQueryItem.value!
                
                if !blackPlayerStr.isEmpty {
                    game.blackPlayer = Player(JSONString:blackPlayerStr)!
                }
                
            }
        }
    }
    
    init?(gameMode: GameMode) {
        self.gameMode = gameMode
        switch gameMode {
        case .RandomMode:
            let position =  Game.Position.init(fen:String.getFENString())
            do {
                try  self.game = Game.init(position: position!, whitePlayer: Player(), blackPlayer: Player(), variant: .standard)
                self.gameMode = gameMode
            } catch let excuError as NSError{
                print(excuError)
                return nil
            }
            
        case .NormalMode:
            game = Game.init(whitePlayer: Player(), blackPlayer: Player(), variant: Variant.standard)
        case .HiddenMode:

            let position =  Game.Position.init(board: Board.init(), playerTurn: Game.PlayerTurn.white, castlingRights: [], enPassantTarget: nil, halfmoves: 0, fullmoves: 1)
            do {
                try  game = Game.init(position: position, whitePlayer: Player(), blackPlayer: Player(), variant: .standard)
                self.gameMode = gameMode
                if (self.hiddenPieceDict == nil) {
                    self.hiddenPieceDict = self.generateHiddenPieceDict()
                }
            } catch let excuError as NSError{
                print(excuError)
                return nil
            }
        }
    }
    
    public func updateForNewUser(udid:String, name: String){
        if (lastPlayerUDID == nil || lastPlayerUDID != udid) {

            if self.game.playerTurn.isWhite {
                if (self.game.whitePlayer.udid == nil) {
                    self.game.whitePlayer.udid = udid
                    self.game.whitePlayer.udid = udid
                }
                currentPlayer = self.game.whitePlayer
                opponentPlayer = self.game.blackPlayer
            }else{
                if (self.game.blackPlayer.udid == nil) {
                    self.game.blackPlayer.udid = udid
                }
                currentPlayer = self.game.blackPlayer
                opponentPlayer = self.game.whitePlayer
            }
            if lastPlayerUDID == nil{
                currentPlayer.countdownTime = 60*10
                opponentPlayer.countdownTime = 60*10
            }
            lastPlayerUDID = udid
        }else{
            if lastPlayerUDID == game.whitePlayer.udid {
                currentPlayer = self.game.whitePlayer
                opponentPlayer = self.game.blackPlayer
            }else{
                currentPlayer = self.game.blackPlayer
                opponentPlayer = self.game.whitePlayer
            }
        }
    }
    
    
    public func isYourTurn()-> Bool{
        return true
        if ( (self.lastPlayerUDID != nil))  {
            
            if (self.game.playerTurn.isWhite && self.lastPlayerUDID == game.whitePlayer.udid) || (self.game.playerTurn.isBlack && self.lastPlayerUDID == game.blackPlayer.udid) {
                return true
            }else{
                return false
            }
        }else{
            return false
        }
    }
    
    public func isWinner()-> Bool{
        if ((loser != nil) && !loser.isEmpty) && (self.lastPlayerUDID != nil){
            if loser != self.lastPlayerUDID {
                return true
            }
        }
        return false
    }
    
    public func updateLoser(){
        loser = lastPlayerUDID
    }
    
    public func isGameFinished()-> Bool{
        return ((loser != nil) && !loser.isEmpty) || game.isFinished
    }
    
    public func getRandomPiece(space: Board.Space)-> Piece!{
        if let pieceHashValue = self.hiddenPieceDict[String(space.square.hashValue)]{
            print(String(space.square.hashValue))
            return Piece(value: pieceHashValue)!
        }
        return nil
    }
    
    private func generateHiddenPieceDict()-> [String: Int]{
        var whitePieceArr = game.board.whitePieces.filter{!$0.kind.isKing}
        var blackPieceArr = game.board.blackPieces.filter{!$0.kind.isKing}
        var pieceDict: [String: Int] = [:]
        
        
        for square in game.board.squares(for: Color.white) {
            if !(game.board.space(at: square).piece?.kind.isKing)! {
                let dice = Int(arc4random_uniform(UInt32(whitePieceArr.count)))
                pieceDict[String(square.hashValue)] = whitePieceArr[dice].hashValue
                whitePieceArr.remove(at: dice)
            }

        }
        
        for square in game.board.squares(for: Color.black){
            if !(game.board.space(at: square).piece?.kind.isKing)!{
                let dice = Int(arc4random_uniform(UInt32(blackPieceArr.count)))
                pieceDict[String(square.hashValue)] = blackPieceArr[dice].hashValue
                blackPieceArr.remove(at: dice)
            }
        }
        return pieceDict
    }
    
    public func updateCapturedPieceStrings(){
        game.whitePlayer.capturedPieces += getCapturedPieceSymbols(playerTurn: Color.black)
        game.blackPlayer.capturedPieces += getCapturedPieceSymbols(playerTurn: Color.white)
    }
    
    private func getCapturedPieceSymbols(playerTurn: Color) -> String {
        var symbolStr = ""
        let pieces = game.capturedPieces(for: playerTurn)
        
        for piece in pieces {
            symbolStr += String(piece.symbol)
        }
        
        return symbolStr
    }
}
