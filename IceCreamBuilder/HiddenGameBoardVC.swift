/*
 Copyright (C) 2016 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information
 
 Abstract:
 The application's view controller.
 */

import UIKit
import Messages
import ExpandingMenu
import SnapKit
class HiddenGameBoardVC: MSMessagesAppViewController,BoardViewDelegate {
    @IBOutlet weak var visibleView: UIView!
    @IBOutlet weak var boardView: BoardView!
    
    var gameViewModel: GameViewModel!
    var onLocationSelectionComplete: ((_ gameState: GameViewModel,_ isresign: Bool, _ snapshot: UIImage) -> Void)?
    var onGameCompletion: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.boardView.viewModel = self.gameViewModel
        self.boardView.delegate = self
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.checkGameStatus()
        }
    }

    
    fileprivate func configureExpandingMenuButton() {
        let menuButtonSize: CGSize = CGSize(width: 64.0, height: 64.0)
        let menuButton = ExpandingMenuButton(frame: CGRect(origin: CGPoint.zero, size: menuButtonSize), centerImage: UIImage(named: "chooser-button-tab")!, centerHighlightedImage: UIImage(named: "chooser-button-tab-highlighted")!)
        self.visibleView.addSubview(menuButton)
        menuButton.snp.makeConstraints { (make) -> Void in
            make.width.size.equalTo(menuButtonSize)
            make.bottom.equalTo(menuButton.superview!).offset(0)
            make.centerX.equalTo(menuButton.superview!)
        }
        
        func showAlert(_ title: String) {
            let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert:UIAlertAction) -> Void in
                self.gameViewModel.updateLoser()
                self.onLocationSelectionComplete?(self.gameViewModel,true, UIImage.snapshot(from: self.boardView))
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
        let    item1 = ExpandingMenuItem(size: menuButtonSize, title: "New game", image: UIImage(named: "chooser-moment-button")!, highlightedImage: UIImage(named: "chooser-moment-button-highlighted")!, backgroundImage: UIImage(named: "chooser-moment-button"), backgroundHighlightedImage: UIImage(named: "chooser-moment-button-highlighted")) { () -> Void in
            if self.gameViewModel.isGameFinished() {
                self.onGameCompletion?()
            }else{
                showAlert("To start new game you need to resign first!")
            }
        }
        
        if self.gameViewModel.isGameFinished() {
            menuButton.addMenuItems([item1])
        }else{
            let    item2 = ExpandingMenuItem(size: menuButtonSize, title: "Resign", image: UIImage(named: "chooser-moment-button")!, highlightedImage: UIImage(named: "chooser-moment-button-highlighted")!, backgroundImage: UIImage(named: "chooser-moment-button"), backgroundHighlightedImage: UIImage(named: "chooser-moment-button-highlighted")) { () -> Void in
                showAlert("Resign")
            }
            
            menuButton.addMenuItems([item1,item2])
        }
    }
    
    func checkGameStatus(){
        self.configureExpandingMenuButton()
        
        if gameViewModel.isGameFinished() {
            congrationView()
        }else{
            self.boardView.isUserInteractionEnabled = self.gameViewModel.isYourTurn()
            if gameViewModel.game.kingIsChecked {
                let banner = Banner(title: "King Is Checked!", subtitle: nil, image: nil, backgroundColor: UIColor(hexString: "#FFC952", alpha: 1))
                banner.springiness = .slight
                banner.dismissesOnTap = true
                banner.show(self.visibleView,duration: 3.0)
            }
        }
    }
    func congrationView()  {
        self.boardView.isUserInteractionEnabled = false;
        if gameViewModel.isWinner() {
            let confettiView = SAConfettiView(frame: self.visibleView.bounds)
            self.visibleView.addSubview(confettiView)
            confettiView.startConfetti()
            confettiView.isUserInteractionEnabled = false;
        }else{
            let banner = Banner(title: "The game is finished!", subtitle: "Start new game by click button (+)", image: nil, backgroundColor: UIColor(hexString: "#FFC952", alpha: 1))
            banner.springiness = .slight
            banner.dismissesOnTap = true
            banner.show(self.visibleView,duration: 3.0)
        }
    }
    func finishMove() {
        if !gameViewModel.isGameFinished() {
            onLocationSelectionComplete?(gameViewModel,false, UIImage.snapshot(from: boardView))
        }else{
            congrationView()
        }
    }
}

