//
//  Dictionary-Extension.swift
//  HiddenChessGame
//
//  Created by Hung Hoang on 12/4/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import Foundation

extension Dictionary{
    func convertToString() -> String!{
        do {
            
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data
            
            
            if let datastring = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String! {
                return datastring
            }
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }
}
