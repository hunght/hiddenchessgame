//
//  GameStartVC.swift
//  Tic Tac Toe for Messages
//
//  Created by Keli'i Martin on 9/17/16.
//  Copyright © 2016 WERUreo. All rights reserved.
//

import UIKit
import LTMorphingLabel

class GameStartVC: UIViewController
{
    var onButtonTap: ((GameViewModel) -> (Void))?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupButtonAnimationView()
        setupTitleLabelAnimation()
    }
    
    @IBOutlet weak var normalGameButton: UIButton!
    @IBOutlet weak var randomGameButton: UIButton!
    @IBOutlet weak var hiddenGameButton: UIButton!
    @IBOutlet weak var titleLable: LTMorphingLabel!
    
    @IBAction func clickedOnHiddenGame(_ sender: Any) {
        let gameViewModel = GameViewModel.init(gameMode: .HiddenMode)
        self.onButtonTap?(gameViewModel!)
    }
    @IBAction func clickedOnRandomGame(_ sender: Any) {
        let gameViewModel = GameViewModel.init(gameMode: .RandomMode)
        self.onButtonTap?(gameViewModel!)
    }
    @IBAction func clickedOnNormalGame(_ sender: Any) {
        let gameViewModel = GameViewModel.init(gameMode: .NormalMode)
        self.onButtonTap?(gameViewModel!)
    }

    func setupTitleLabelAnimation() {
        let index = Int(arc4random_uniform(UInt32(LTMorphingEffect.allValues.count)))

        if let effect = LTMorphingEffect(rawValue: index) {
            titleLable.morphingEffect = effect
        }
        if self.titleLable.text == "🔥Chess Pro Mode🔥" {
            self.titleLable.text = "Play like a Pro"
        }else{
            self.titleLable.text = "🔥Chess Pro Mode🔥"
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.setupTitleLabelAnimation()
        }
    }
    
    func setupButtonAnimationView() {
        self.normalGameButton.layer.cornerRadius = 5.0
        self.randomGameButton.layer.cornerRadius = 5.0
        self.hiddenGameButton.layer.cornerRadius = 5.0
        
        UIView.animate(withDuration: 0.5, delay: 0.5,
                       options: [.repeat, .autoreverse, .curveEaseOut,.allowUserInteraction],
                       animations: {
                        self.normalGameButton.layer.position.x += 5
                        self.hiddenGameButton.layer.position.x += 5
        }, completion: nil)
        
        UIView.animate(withDuration: 0.5, delay: 0,
                       options: [.repeat, .autoreverse, .curveEaseOut,.allowUserInteraction],
                       animations: {
                        self.randomGameButton.layer.position.x += 5
        }, completion: nil)
    }
}
