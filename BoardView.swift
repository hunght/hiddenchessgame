//
//  BoardView.swift
//  HiddenChessGame
//
//  Created by Hung Hoang on 11/1/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

protocol BoardViewDelegate{
    func finishMove()
}

class  BoardView: UIView, CountdownLabelDelegate{

    private var _viewModel: GameViewModel!
    var viewModel: GameViewModel!{
        set{
            _viewModel = newValue
            self.setupContentView()
        }
        get{
            return _viewModel
        }
    }
    // Our custom view from the XIB file
    var view: UIView!
    var pieceButtonArr: [SpaceView]!
    var recentPieceButtonArr: [SpaceView]!
    var lastTouchedSpaceView: SpaceView!
    var delegate:BoardViewDelegate! = nil
    var wasSetupContentView:Bool = false
    var wasLayoutSubviews:Bool = false
    
    @IBOutlet weak var playerCapturedPiecesLabel: UILabel!
    @IBOutlet weak var opponentCapturedPiecesLabel: UILabel!
    @IBOutlet weak var opponentTimeLabel: CountdownLabel!
    @IBOutlet weak var playerTimeLable: CountdownLabel!
    @IBOutlet weak var contentView: UIView!
    override init(frame:CGRect) {
        super.init(frame:frame)
        xibSetup()
    }
    required init(coder aDecoder:NSCoder) {
        super.init(coder:aDecoder)!
        xibSetup()
    }
    
    func xibSetup() {
        pieceButtonArr = []
        recentPieceButtonArr = []
        
        view = loadViewFromNib()
        
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        view.snp.makeConstraints { (make) -> Void in
            make.edges.equalTo(self).inset(UIEdgeInsetsMake(0, 0, 0, 0))
        }
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: BoardView.self), bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    private func setupContentView() {
        if (contentView != nil) && (viewModel != nil) && !wasSetupContentView && wasLayoutSubviews{
            wasSetupContentView = true
            opponentCapturedPiecesLabel.layer.cornerRadius = 5
            playerCapturedPiecesLabel.layer.cornerRadius = 5
            let isRotate = viewModel.isYourTurn() && viewModel.game.playerTurn.isBlack
            let spaceSize: CGFloat = self.bounds.width/8
            for space in viewModel.game.board {
                let spaceView = self.getBoardView(space: space, size:spaceSize)
                if isRotate {
                    spaceView.transform = CGAffineTransform( rotationAngle: CGFloat(M_PI));
                }
                contentView.addSubview(spaceView)
            }
            
            if isRotate {
                contentView.transform = CGAffineTransform( rotationAngle: CGFloat(M_PI));
            }
            
            if let timeInterval = viewModel.currentPlayer.countdownTime {
                playerTimeLable.countdownDelegate = self;
                playerTimeLable.addTime(time: TimeInterval(timeInterval))
                let index = Int(arc4random_uniform(UInt32(CountdownEffect.count)))
                playerTimeLable.animationType = CountdownEffect(rawValue:index)
                playerTimeLable.timeFormat = "mm:ss"
                playerTimeLable.start()
                // then property
                let _ = playerTimeLable.then(targetTime: 10) { [unowned self] in
                    self.playerTimeLable.animationType = .Pixelate
                    self.playerTimeLable.textColor = .green
                }
                let _ = playerTimeLable.then(targetTime: 5) { [unowned self] in
                    self.playerTimeLable.animationType = .Sparkle
                    self.playerTimeLable.textColor = .yellow
                }
            }
            
            if let timeInterval = viewModel.opponentPlayer.countdownTime {
                
                opponentTimeLabel.addTime(time: TimeInterval(timeInterval))
                opponentTimeLabel.timeFormat = "mm:ss"
            }

            opponentCapturedPiecesLabel.text = viewModel.opponentPlayer.capturedPieces
            
            playerCapturedPiecesLabel.text = viewModel.currentPlayer.capturedPieces
        }
    }
    
    func countingAt(timeCounted: TimeInterval, timeRemaining: TimeInterval) {
        viewModel.currentPlayer.countdownTime = Int( timeRemaining)
        if  viewModel.currentPlayer.countdownTime == 0 {
            viewModel.loser = viewModel.currentPlayer.udid
            self.delegate.finishMove()
        }
    }
    func countdownStarted(){}
    func countdownPaused(){}
    func countdownFinished(){}
    func countdownCancelled(){}
    
    override func layoutSubviews() {
        super.layoutSubviews()
        wasLayoutSubviews = true
        self.setupContentView()
    }
    
    private func bindViewModel(){

    }

    internal func getBoardView(space: Board.Space, size: CGFloat) -> UIView {
        
        let rectY = CGFloat(7 - space.rank.index) * size
        let frame = CGRect(x: CGFloat(space.file.index) * size,
                           y: rectY,
                           width: size,
                           height: size)
        let pieceFrame = CGRect(x: 0, y: 0, width: size, height: size)
        let view = UIView(frame: frame)

        view.backgroundColor = space.color.isBlack ? UIColor(red: 160.0/255, green:120.0/255, blue:55.0/255, alpha:1) : UIColor(red: 222.0/255, green:196.0/255, blue:160.0/255, alpha:1)
        var spaceView:SpaceView
        if viewModel.gameMode == .HiddenMode && space.piece != nil {
            spaceView = SpaceView(frame: pieceFrame, viewModel: space, hiddenPiece:viewModel.getRandomPiece(space: space))
        }else{
            spaceView = SpaceView(frame: pieceFrame, viewModel: space, hiddenPiece:nil)
        }
        
        spaceView.addTarget(self, action: #selector(pressed(sender:)), for: UIControlEvents.touchUpInside)
        
        view.addSubview(spaceView)
        pieceButtonArr.append(spaceView)
        return view
    }
    
    func pressed(sender: SpaceView!) {
        for button in recentPieceButtonArr{
            button.backgroundColor = UIColor.clear
        }
        recentPieceButtonArr.removeAll()
        let space = viewModel.game.board.space(at: sender.square)
        recentPieceButtonArr.append(sender)
        if let piece = space.piece{
            if piece.color == viewModel.game.playerTurn {
                let moveArr = viewModel.game.movesForPiece(at: sender.square)
                if moveArr.count > 0{
                    for move in moveArr{
                        
                        let button = pieceButtonArr[move.end.rawValue]
                        button.backgroundColor = UIColor.lightGray.withAlphaComponent(0.7)
                        recentPieceButtonArr.append(button)
                    }
                    sender.backgroundColor = UIColor.gray
                    lastTouchedSpaceView = sender
                }
            }
        }
        
        if (lastTouchedSpaceView != nil && lastTouchedSpaceView != sender)  {
            let move = Move(start: lastTouchedSpaceView.square, end: sender.square)
            
            do {
                try viewModel.game.execute(move: move)

                self.checkGameAfter(move: move, currentSpaceView: sender)
            } catch let excuError as NSError {
               //
                print(excuError)
            } catch {
                // Catch any other errors 
            }
            lastTouchedSpaceView = nil;
        }
    }
    
    func checkGameAfter(move:Move, currentSpaceView:SpaceView) {
        if move.isCastle() {
            var start = move.start.rawValue
            var end = move.end.rawValue
            if start > end {
                end = end - 2
                let startTemp = start
                start = end
                end = startTemp
            }else{
                end = end + 2
            }
            for i in start...end {
                if pieceButtonArr.indices.contains(i){
                    let spaceView = pieceButtonArr[i]
                    spaceView.updateView(space: viewModel.game.board.space(at: spaceView.square))
                }
            }
        }else{
            
            lastTouchedSpaceView.updateView(space: viewModel.game.board.space(at: lastTouchedSpaceView.square))
            if viewModel.gameMode == .HiddenMode{
                if lastTouchedSpaceView.hiddenPiece != nil{
                    viewModel.game.board.removePiece(at: currentSpaceView.square)
                    viewModel.game.board[currentSpaceView.square] = lastTouchedSpaceView.hiddenPiece
                    if !lastTouchedSpaceView.hiddenPiece.kind.isPawn {
                        viewModel.game.enPassantTarget = nil
                    }
                    lastTouchedSpaceView.hiddenPiece = nil
                    viewModel.hiddenPieceDict[String(lastTouchedSpaceView.square.hashValue)] = nil
                }
                
                if (currentSpaceView.hiddenPiece != nil){
                    currentSpaceView.hiddenPiece = nil
                    viewModel.hiddenPieceDict[String(currentSpaceView.square.hashValue)] = nil
                }
            }
            currentSpaceView.updateView(space: viewModel.game.board.space(at: currentSpaceView.square))
        }
        
        viewModel.updateCapturedPieceStrings()
        opponentCapturedPiecesLabel.text = viewModel.opponentPlayer.capturedPieces
        
        playerCapturedPiecesLabel.text = viewModel.currentPlayer.capturedPieces
        delegate!.finishMove()
    }
    
    
}
