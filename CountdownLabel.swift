//
//  CountdownLabel.swift
//  CountdownLabel
//
//  Created by suzuki keishi on 2016/01/06.
//  Copyright © 2016 suzuki_keishi. All rights reserved.
//

import UIKit
import LTMorphingLabel

@objc public protocol CountdownLabelDelegate {
    func countdownStarted()
    func countdownPaused()
    func countdownFinished()
    func countdownCancelled()
    func countingAt(timeCounted: TimeInterval, timeRemaining: TimeInterval)
}

public extension TimeInterval {
    var int: Int {
        return Int(self)
    }
}

public class CountdownLabel: LTMorphingLabel {
    
    public typealias CountdownCompletion = () -> ()?
    public typealias CountdownExecution = () -> ()
     let defaultFireInterval = 1.0
     let date1970 = NSDate(timeIntervalSince1970: 0)
    
    // conputed property
    public var dateFormatter: DateFormatter {
        let df = DateFormatter()
        df.locale = NSLocale.current
        df.timeZone = NSTimeZone(name: "GMT") as TimeZone!
        df.dateFormat = timeFormat
        return df
    }
    
    public var timeCounted: TimeInterval {
        let timeCounted = NSDate().timeIntervalSince(fromDate as Date)
        return round(timeCounted < 0 ? 0 : timeCounted)
    }
    
    public var timeRemaining: TimeInterval {
        return round(currentTime) - timeCounted
    }
    
    public var isPaused: Bool {
        return paused
    }
    
    public var isCounting: Bool {
        return counting
    }
    
    public var isFinished: Bool {
        return finished
    }
    
    public weak var countdownDelegate: CountdownLabelDelegate?
    
    // user settings
    public var animationType: CountdownEffect? {
        didSet {
            if let effect = animationType?.toLTMorphing() {
                morphingEffect = effect
                morphingEnabled = true
            } else {
                morphingEnabled = false
            }
        }
    }
    public var timeFormat = "HH:mm:ss"
    public var thens = [TimeInterval: CountdownExecution]()
    public var countdownAttributedText: CountdownAttributedText! {
        didSet {
            range = (countdownAttributedText.text as NSString).range(of: countdownAttributedText.replacement)
        }
    }
    
     var completion: CountdownCompletion?
     var fromDate: NSDate = NSDate()
     var currentDate: NSDate = NSDate()
     var currentTime: TimeInterval = 0
     var diffDate: NSDate!
     var targetTime: TimeInterval = 0
     var pausedDate: NSDate!
     var range: NSRange!
     var timer: Timer!
    
     var counting: Bool = false
     var endOfTimer: Bool {
        return timeCounted >= currentTime
    }
     var finished: Bool = false {
        didSet {
            if finished {
                paused = false
                counting = false
            }
        }
    }
     var paused: Bool = false
    
    // MARK: - Initialize
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    public override required init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    public convenience init(frame: CGRect, minutes: TimeInterval) {
        self.init(frame: frame)
        setCountDownTime(minutes: minutes)
    }
    
    public convenience init(frame: CGRect, date: NSDate) {
        self.init(frame: frame)
        setCountDownDate(targetDate: date)
    }
    
    public convenience init(frame: CGRect, fromDate: NSDate, targetDate: NSDate) {
        self.init(frame: frame)
        setCountDownDate(fromDate: fromDate, targetDate: targetDate)
    }
    
    deinit {
        dispose()
    }
    
    // MARK: - Setter Methods
    public func setCountDownTime(minutes: TimeInterval) {
        setCountDownTime(fromDate: NSDate(), minutes: minutes)
    }
    
    public func setCountDownTime(fromDate: NSDate, minutes: TimeInterval) {
        self.fromDate = fromDate
        
        targetTime = minutes
        currentTime = minutes
        diffDate = date1970.addingTimeInterval(minutes)
        
        updateLabel()
    }
    
    public func setCountDownDate(targetDate: NSDate) {
        setCountDownDate(fromDate: NSDate(), targetDate: targetDate)
    }
    
    public func setCountDownDate(fromDate: NSDate, targetDate: NSDate) {
        self.fromDate = fromDate
        
        targetTime = targetDate.timeIntervalSince(fromDate as Date)
        currentTime = targetDate.timeIntervalSince(fromDate as Date) 
        diffDate = date1970.addingTimeInterval(targetTime)
        
        updateLabel()
    }
    
    // MARK: - Update
    func updateLabel() {
        // delegate
        countdownDelegate?.countingAt(timeCounted: timeCounted, timeRemaining: timeRemaining)
        
        // then function execute if needed
        thens.forEach { k, v in
            if k.int == timeRemaining.int {
                v()
                thens[k] = nil
            }
        }
        
        // update text
        updateText()
        
        // if end of timer
        if endOfTimer {
            text = dateFormatter.string(from: date1970.addingTimeInterval(0) as Date)
            countdownDelegate?.countdownFinished()
            dispose()
            completion?()
        }
    }
}

// MARK: - Public
extension CountdownLabel {
    func start(completion: ( () -> () )? = nil) {
        if !isPaused {
            // current date should be setted at the time of the counter's starting, or the time will be wrong (just a few seconds) after the first time of pausing.
            currentDate = NSDate()
        }
        
        // pause status check
        updatePauseStatusIfNeeded()
        
        // create timer
        updateTimer()
        
        // fire!
        timer.fire()
        
        // set completion if needed
        completion?()
        
        // set delegate
        countdownDelegate?.countdownStarted()
    }
    
    func pause(completion: (() -> ())? = nil) {
        if paused {
            return
        }
        
        // invalidate timer
        disposeTimer()
        
        // stop counting
        counting = false
        paused = true
        
        // reset
        pausedDate = NSDate()
        
        // set completion if needed
        completion?()
        
        // set delegate
        countdownDelegate?.countdownPaused()
    }
    
    func cancel(completion: (() -> ())? = nil) {
        text = dateFormatter.string(from: date1970.addingTimeInterval(0) as Date)
        dispose()
        
        // set completion if needed
        completion?()
        
        // set delegate
        countdownDelegate?.countdownCancelled()
    }
    
    func addTime(time: TimeInterval) {
        currentTime = time + currentTime
        diffDate = date1970.addingTimeInterval(currentTime)
        
        updateLabel()
    }
    
    func then(targetTime: TimeInterval, completion: @escaping () -> ()) -> Self {
        let t = targetTime - (targetTime - targetTime)
        guard t > 0 else {
            return self
        }
        
        thens[t] = completion
        return self
    }
}

// MARK: - private
extension CountdownLabel {
    func setup() {
        morphingEnabled = false
    }
    
    func updateText() {
        guard diffDate != nil else { return }

        // if time is before start
        let formattedText = timeCounted < 0
            ? dateFormatter.string(from: date1970.addingTimeInterval(0) as Date)
            : dateFormatter.string(from: diffDate.addingTimeInterval(round(timeCounted * -1)) as Date)
        
        if let countdownAttributedText = countdownAttributedText {
            let attrTextInRange = NSAttributedString(string: formattedText, attributes: countdownAttributedText.attributes)
            let attributedString = NSMutableAttributedString(string: countdownAttributedText.text)
            attributedString.replaceCharacters(in: range, with: attrTextInRange)
            
            attributedText = attributedString
            text = attributedString.string
        } else {
            text = formattedText
        }
        setNeedsDisplay()
    }
    
    func updatePauseStatusIfNeeded() {
        guard paused else {
            return
        }
        // change date
        let pastedTime = pausedDate.timeIntervalSince(currentDate as Date)
        currentDate = NSDate().addingTimeInterval(-pastedTime)
        fromDate = currentDate
        
        // reset pause
        pausedDate = nil
        paused = false
    }
    
    func updateTimer() {
        disposeTimer()
        
        // create
        timer = Timer.scheduledTimer(timeInterval: defaultFireInterval,
                                                       target: self,
                                                       selector: #selector(updateLabel),
                                                       userInfo: nil,
                                                       repeats: true)
        
        // register to NSrunloop
        RunLoop.current.add(timer, forMode: RunLoopMode.commonModes)
        counting = true
    }
    
    func disposeTimer() {
        if timer != nil {
            timer.invalidate()
            timer = nil
        }
    }
    
    func dispose() {
        // reset
        pausedDate = nil
        
        // invalidate timer
        disposeTimer()
        
        // stop counting
        finished = true
    }
}

public enum CountdownEffect:Int {
    case Anvil
    case Burn
    case Evaporate
    case Fall
    case None
    case Pixelate
    case Scale
    case Sparkle
    static var count: Int { return CountdownEffect.Sparkle.hashValue + 1}
    
    func toLTMorphing() -> LTMorphingEffect? {
        switch self {
        case .Anvil     : return .anvil
        case .Burn      : return .burn
        case .Evaporate : return .evaporate
        case .Fall      : return .fall
        case .None      : return nil
        case .Pixelate  : return .pixelate
        case .Scale     : return .scale
        case .Sparkle   : return .sparkle
        }
    }
}

public class CountdownAttributedText: NSObject {
     let text: String
     let replacement: String
     let attributes: [String: AnyObject]?
   
    public init(text: String, replacement: String, attributes: [String: AnyObject]? = nil) {
        self.text = text
        self.replacement = replacement
        self.attributes = attributes
    }
}
