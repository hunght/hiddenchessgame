//
//  UIColor-Extention.swift
//  HiddenChessGame
//
//  Created by Hung Hoang on 11/15/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
extension UIColor {
    convenience init(hexString: String, alpha: Float) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            ( r, g, b) = ( (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            ( r, g, b) = (int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (r, g, b) = (int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (r, g, b) = (0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(alpha))
    }
    
    convenience init(hexString: String){
        self.init(hexString:hexString, alpha:1.0)
    }
}
